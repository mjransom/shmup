import pygame
import random
from os import path
from time import sleep


img_dir = path.join(path.dirname(__file__), "img")
snd_dir = path.join(path.dirname(__file__), "snd")



WIDTH = 480
HEIGHT = 600
FPS = 120
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)

pygame.init()
pygame.mixer.init()
pygame.font.init()

#screen = pygame.display.set_mode((WIDTH, HEIGHT))
screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
pygame.display.set_caption("My Game")
clock = pygame.time.Clock()
WIDTH = pygame.display.Info().current_w
HEIGHT = pygame.display.Info().current_h


background_img = pygame.image.load(path.join(img_dir, "starfield.png")).convert()
background = pygame.transform.scale(background_img, (WIDTH, HEIGHT))
background_rect = background.get_rect()
player_img = pygame.image.load(path.join(img_dir, "player.png")).convert()
health_img = pygame.image.load(path.join(img_dir, "health.png")).convert()
bounus_img = pygame.image.load(path.join(img_dir, "bounus.png")).convert()
mystery_img = pygame.image.load(path.join(img_dir, "mystery.png")).convert()
gun2_img = pygame.image.load(path.join(img_dir, "gunx2.png")).convert()
gun4_img = pygame.image.load(path.join(img_dir, "gunx4.png")).convert()

#meteor_img = pygame.image.load(path.join(img_dir, "MeteorBig.png")).convert()
bullet_img = pygame.image.load(path.join(img_dir, "laserRed.png")).convert()
meteor_img_list = []
meteor_img_list.append(pygame.image.load(path.join(img_dir, "MeteorBig.png")).convert())
meteor_img_list.append(pygame.image.load(path.join(img_dir, "MeteorSmall.png")).convert())
meteor_img_list.append(pygame.image.load(path.join(img_dir, "spaceMeteors_001.png")).convert())
meteor_img_list.append(pygame.image.load(path.join(img_dir, "enemyUFO.png")).convert())

shoot_snd = pygame.mixer.Sound(path.join(snd_dir, "Laser_Shoot.wav"))
explosion_snd = pygame.mixer.Sound(path.join(snd_dir, "Explosion.wav"))
death_snd = pygame.mixer.Sound(path.join(snd_dir, "Explosion_Death.wav"))
health_snd = pygame.mixer.Sound(path.join(snd_dir, "Health_up.wav"))
bounus_snd = pygame.mixer.Sound(path.join(snd_dir, "bounus.wav"))
new_life_snd = pygame.mixer.Sound(path.join(snd_dir, "New_Life.wav"))
new_gun_snd = pygame.mixer.Sound(path.join(snd_dir, "new_gun.wav"))

music = pygame.mixer.music.load(path.join(snd_dir, "tgfcoder-FrozenJam-SeamlessLoop.ogg"))
pygame.mixer.music.set_volume(0.4)

with open("highscore","r") as f:
    highscore = f.read()


font_name = "/Library/Fonts/Microsoft/Arial.ttf"
life_list = []
def draw_text(surf, text, size, x, y):
    font = pygame.font.Font(font_name, size)
    text_surface = font.render(text, True, WHITE)
    text_rect = text_surface.get_rect()
    text_rect.midtop = (x, y)
    surf.blit(text_surface, text_rect)

def draw_sheild_bar(surf, x, y, pct):
    if pct < 0:
        pct = 0
    BAR_LEN = 100
    BAR_HEIGHT = 10
    fill = (pct / 100 * BAR_LEN)
    outline_rect = pygame.Rect(x, y, BAR_LEN, BAR_HEIGHT)
    fill_rect = pygame.Rect(x, y, fill, BAR_HEIGHT)
    if pct >= 50:
        pygame.draw.rect(surf, GREEN, fill_rect)
        pygame.draw.rect(surf, WHITE, outline_rect, 2)
    elif pct < 50 and pct >= 35:
        pygame.draw.rect(surf, YELLOW, fill_rect)
        pygame.draw.rect(surf, WHITE, outline_rect, 2)

    else:
        pygame.draw.rect(surf, RED, fill_rect)
        pygame.draw.rect(surf, WHITE, outline_rect, 2)


class Health(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((50, 40))
        self.image = pygame.transform.scale(health_img, (50, 38))
        self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()
        self.org_image = pygame.transform.scale(health_img, (50, 30))
        # pygame.draw.circle(self.image, RED, self.rect.center, self.radius)
        self.rect.x = random.randint(0, WIDTH - self.rect.width)
        self.rect.y = random.randint(-100, -40)
        # self.speedx = 0
        self.radius = 20
        self.speedy = random.randint(1, 8)
        self.speedx = random.randint(-3, 3)



    def update(self):
        self.rect.y += self.speedy
        self.rect.x += self.speedx
        if self.rect.left < 0:
            self.speedx *= -1
        if self.rect.right >= WIDTH:
            self.speedx *= -1
        if self.rect.top > HEIGHT + 10:
            self.kill()
            # self.rect.x = random.randint(0, WIDTH - self.rect.width)
            # self.rect.y = random.randint(-100, -40)
            # self.speedy = random.randint(1, 8)


class Gun(pygame.sprite.Sprite):
    def __init__(self,type=2):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((50, 40))
        self.type = type
        if self.type == 2:
            self.image = pygame.transform.scale(gun2_img, (50, 38))
            self.image.set_colorkey(BLACK)
            self.rect = self.image.get_rect()
            self.org_image = pygame.transform.scale(gun2_img, (50, 30))
        if self.type == 4:
            self.image = pygame.transform.scale(gun4_img, (50, 38))
            self.image.set_colorkey(BLACK)
            self.rect = self.image.get_rect()
            self.org_image = pygame.transform.scale(gun4_img, (50, 30))

        # pygame.draw.circle(self.image, RED, self.rect.center, self.radius)
        self.rect.x = random.randint(0, WIDTH - self.rect.width)
        self.rect.y = random.randint(-100, -40)
        # self.speedx = 0
        self.radius = 20
        self.speedy = random.randint(1, 8)
        self.speedx = random.randint(-3, 3)



    def update(self):
        self.rect.y += self.speedy
        self.rect.x += self.speedx
        if self.rect.left < 0:
            self.speedx *= -1
        if self.rect.right >= WIDTH:
            self.speedx *= -1
        if self.rect.top > HEIGHT + 10:
            self.kill()
            # self.rect.x = random.randint(0, WIDTH - self.rect.width)
            # self.rect.y = random.randint(-100, -40)
            # self.speedy = random.randint(1, 8)


class Life(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        #self.image = pygame.Surface((50, 40))
        self.image = pygame.transform.scale(player_img, (30, 15))
        self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()
        self.org_image = pygame.transform.scale(player_img, (30, 15))
        self.rect.left = 10
        self.rect.bottom = 55


    def update(self):
        pass
        # self.rect.y += self.speedy
        # self.rect.x += self.speedx
        # if self.rect.left < 0:
        #     self.speedx *= -1
        # if self.rect.right >= WIDTH:
        #     self.speedx *= -1
        # if self.rect.top > HEIGHT + 10:
        #     self.kill()
        #     # self.rect.x = random.randint(0, WIDTH - self.rect.width)
        #     # self.rect.y = random.randint(-100, -40)
        #     # self.speedy = random.randint(1, 8)


class Mystery(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((50, 40))
        self.image = pygame.transform.scale(mystery_img, (50, 38))
        self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()
        self.org_image = pygame.transform.scale(mystery_img, (50, 30))
        # pygame.draw.circle(self.image, RED, self.rect.center, self.radius)
        self.rect.x = random.randint(0, WIDTH - self.rect.width)
        self.rect.y = random.randint(-100, -40)
        # self.speedx = 0
        self.radius = 20
        self.speedy = random.randint(1, 8)
        self.speedx = random.randint(-3, 3)
        type_list = ["bounus", "damage", "health", "life"]
        if len(life_list) < 4:
            type_chance = random.randint(0, 3)
        else:
            type_chance = random.randint(0, 2)
        self.type = type_list[type_chance]

    def update(self):
        self.rect.y += self.speedy
        self.rect.x += self.speedx
        if self.rect.left < 0:
            self.speedx *= -1
        if self.rect.right >= WIDTH:
            self.speedx *= -1
        if self.rect.top > HEIGHT + 10:
            self.kill()


class Bounus(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((50, 40))
        self.image = pygame.transform.scale(bounus_img, (50, 38))
        self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()
        self.org_image = pygame.transform.scale(bounus_img, (50, 30))
        self.rect.x = random.randint(0, WIDTH - self.rect.width)
        self.rect.y = random.randint(-100, -40)
        self.radius = 20
        self.speedy = random.randint(1, 8)
        self.speedx = random.randint(-3, 3)



    def update(self):
        self.rect.y += self.speedy
        self.rect.x += self.speedx
        if self.rect.left < 0:
            self.speedx *= -1
        if self.rect.right >= WIDTH:
            self.speedx *= -1
        if self.rect.top > HEIGHT + 10:
            self.kill()


class Player(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((50, 40))
        self.image = pygame.transform.scale(player_img, (50, 38))
        self.image.set_colorkey(BLACK)
        self.radius = 20
        self.sheild = 100
        self.gun_type = 1
        self.rect = self.image.get_rect()
        #pygame.draw.circle(self.image, RED, self.rect.center, self.radius)
        self.rect.centerx = WIDTH / 2
        self.rect.bottom = HEIGHT - 10
        self.speed = 15
        self.speedy = 0
        self.count = 0
        self.speedx = 0
        self.fire_limit = pygame.time.get_ticks()
        self.gun_timer = pygame.time.get_ticks()

    def update(self):
        now = pygame.time.get_ticks()
        # self.speedx = random.randint(0, 480)
        self.speedy = 0
        self.speedx = 0

        keystate = pygame.key.get_pressed()
        if keystate[pygame.K_LEFT] and self.rect.left > 0:
            self.speedx = self.speed * -1
        if self.rect.left < 0:
            self.rect.left = 0
        if keystate[pygame.K_RIGHT] and self.rect.right < WIDTH:
            self.speedx = self.speed

        if keystate[pygame.K_SPACE]:
            if now - self.fire_limit > 250:
                self.fire_limit = now
                self.shoot()

        if self.rect.right > WIDTH:
            self.rect.right = WIDTH
        self.rect.x += self.speedx
        if keystate[pygame.K_UP] and self.rect.top > 0:
            self.speedy = -20
        if self.rect.top < 0:
            self.rect.top = 0
        if keystate[pygame.K_DOWN] and self.rect.bottom < HEIGHT:
            self.speedy = 20
        if self.rect.bottom > HEIGHT:
            self.rect.bottom = HEIGHT
        self.rect.x += self.speedx
        self.rect.y += self.speedy

    def shoot(self):
        now = pygame.time.get_ticks()
        if now - self.gun_timer > 15000:
            self.gun_type = 1

        if self.gun_type == 1:
            self.gun_timer = pygame.time.get_ticks()
            bullet = Bullet(self.rect.centerx, self.rect.top)
            all_sprites.add(bullet)
            bullets.add(bullet)
            shoot_snd.play()
            self.speed = 15

        if self.gun_type == 2:
            bullet = Bullet(self.rect.centerx - 10, self.rect.top)
            bullet2 = Bullet(self.rect.centerx + 10, self.rect.top)
            all_sprites.add(bullet)
            all_sprites.add(bullet2)
            bullets.add(bullet)
            bullets.add(bullet2)
            shoot_snd.play()
            self.speed = 10

        if self.gun_type == 4:
            bullet = Bullet(self.rect.centerx - 35, self.rect.top)
            bullet2 = Bullet(self.rect.centerx - 15, self.rect.top)
            bullet3 = Bullet(self.rect.centerx + 15, self.rect.top)
            bullet4 = Bullet(self.rect.centerx + 35, self.rect.top)
            all_sprites.add(bullet)
            all_sprites.add(bullet2)
            all_sprites.add(bullet3)
            all_sprites.add(bullet4)
            bullets.add(bullet)
            bullets.add(bullet2)
            bullets.add(bullet3)
            bullets.add(bullet4)
            shoot_snd.play()
            self.speed = 5


class Mob(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((30, 40))
        #self.image.fill(RED)
        #self.image = meteor_img
        if level < 3:
            self.pic = random.randint(0, 2)
        else:
            self.pic = random.randint(0, 3)

        meteor_img = meteor_img_list[self.pic]
        self.org_image = pygame.transform.scale(meteor_img, (50,30))
        self.org_image.set_colorkey(BLACK)
        self.image = self.org_image.copy()



        self.rect = self.image.get_rect()
        self.radius = 20
        #pygame.draw.circle(self.image, RED, self.rect.center, self.radius)
        self.rect.x = random.randint(0, WIDTH - self.rect.width)
        self.rect.y = random.randint(-100, -40)
        #self.speedx = 0
        self.speedy = random.randint(1, 8)
        self.speedx = random.randint(-3, 3)
        self.rot = 0
        self.rot_speed = random.randint(-8, 8)
        self.last_udpate = pygame.time.get_ticks()

    def rotate(self):
        now = pygame.time.get_ticks()
        if self.pic != 3:
            if now - self.last_udpate > 50:
                self.last_udpate = now
                self.rot =  (self.rot + self.rot_speed) % 360
                new_image = pygame.transform.rotate(self.org_image,self.rot)
                old_center = self.rect.center
                self.image = new_image
                self.rect = self.image.get_rect()
                self.rect.center = old_center


    def update(self, *args):
        self.rotate()
        if self.pic == 3 and level < 4:
            self.speedy = 8
        if self.pic == 3 and level == 4:
            self.speedy = 15
        if self.pic == 3 and level >= 5:
            self.speedy = 20
        self.rect.y += self.speedy
        self.rect.x += self.speedx
        if self.rect.left < 0:
            self.speedx *= -1
        if self.rect.right >= WIDTH:
            self.speedx *= -1
        if self.rect.top > HEIGHT + 10:
            self.rect.x = random.randint(0, WIDTH - self.rect.width)
            self.rect.y = random.randint(-100, -40)
            # self.speedx = 0
            self.speedy = random.randint(1, 8)
        # mobs.remove(self)
        # if pygame.sprite.spritecollide(self,mobs,False,pygame.sprite.collide_circle ):
        #     self.speedx *= -1
        #     #self.speedy *= -1
        # mobs.add(self)


class Bullet(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        #self.image = pygame.Surface((10, 20))
        #self.image.fill(YELLOW)
        self.image = bullet_img
        self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()
        self.rect.bottom =y
        self.rect.centerx = x
        self.speedy = -10


    def update(self):
        self.rect.y += self.speedy
        if self.rect.y < 0:

            self.kill()


score = 0
alive = True
level = 1
life_pos = 10
shield_time = 0
bullets = pygame.sprite.Group()
health_ups = pygame.sprite.Group()
bounus_group = pygame.sprite.Group()
bounus = Bounus()
bounus_group.add(bounus)
mystery = Mystery()
mystery_group = pygame.sprite.Group()
mystery_group.add(mystery)
gun = Gun()
gun_group = pygame.sprite.Group()
gun_group.add(gun)


all_sprites = pygame.sprite.Group()
for x in range(0, 3):
    life = Life()
    life.rect.left = life_pos
    all_sprites.add(life)
    life_list.append(life)
    life_pos += 30

mobs = pygame.sprite.Group()
player = Player()
health_up = Health()
health_ups.add(health_up)
all_sprites.add(player)
all_sprites.add(life)
#all_sprites.add(health_up)

for i in range(0, level * 5):
    m = Mob()
    all_sprites.add(m)
    mobs.add(m)

running = True
pygame.mixer.music.play(-1)
while running:
    clock.tick(FPS)


# Events
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_q:
                with open("highscore", "r") as f:
                    hs = f.read()

                if score > int(hs):
                    with open("highscore", "w") as f:
                        f.write(str(score))
                running = False
            elif event.key == pygame.K_SPACE and player.alive():
                player.shoot()
            elif event.key == pygame.K_s and not player.alive():
                mobs = pygame.sprite.Group()
                all_sprites = pygame.sprite.Group()
                level = 1
                score = 0
                for i in range(0, level * 5):
                    m = Mob()
                    all_sprites.add(m)
                    mobs.add(m)
                player = Player()
                all_sprites.add(player)
                pygame.mixer.music.play(-1)
                alive = True
                life_pos = 10
                player.gun_type = 1
                for x in range(0, 3):
                    life = Life()
                    life.rect.left = life_pos
                    all_sprites.add(life)
                    life_list.append(life)
                    life_pos += 30

# Update

# Bullets Check
    hits = pygame.sprite.groupcollide(mobs, bullets, True, True)
    mystery_hit = pygame.sprite.groupcollide(mystery_group, bullets, True, True)
    if hits and player.alive() or mystery_hit and player.alive():
        score += len(hits)
        explosion_snd.play()
        if score > int(highscore):
            highscore = score

    if len(mobs) == 0:
        level += 1
        for i in range(0, level * 10):
            m = Mob()
            all_sprites.add(m)
            mobs.add(m)
        mystery = Mystery()
        mystery_group.add(mystery)
        all_sprites.add(mystery)
        health_up = Health()
        all_sprites.add(health_up)
        health_ups.add(health_up)



# Health check

    hits = pygame.sprite.spritecollide(player, health_ups, True, pygame.sprite.collide_circle)
    if hits:
        player.sheild *= 1.75
        if player.sheild > 100:
            player.sheild = 100
        health_snd.play()

# Gun check

    hits = pygame.sprite.spritecollide(player, gun_group, True, pygame.sprite.collide_circle)
    if hits:
        new_gun_snd.play()
        if gun.type == 2:
            player.gun_type = 2
        if gun.type == 4:
            player.gun_type = 4


# Bounus Check
    hits = pygame.sprite.spritecollide(player, bounus_group, True, pygame.sprite.collide_circle)
    if hits and player.alive():
        score += 200
        bounus_snd.play()

# Mystery Check
    hits = pygame.sprite.spritecollide(player, mystery_group, True, pygame.sprite.collide_circle)

    if hits and player.alive() and mystery.type == "life":
        if len(life_list) < 4:
            new_life = Life()
            new_life.rect.left = life_pos
            life_list.append(new_life)
            all_sprites.add(new_life)
            life_pos += 30
            new_life_snd.play()

    if hits and player.alive() and mystery.type == "health":
        amount = 100 - player.sheild
        player.sheild += amount
        health_snd.play()

    elif hits and player.alive() and mystery.type == "damage":
        player.sheild -= 50
        death_snd.play()

        if hits and player.alive() and player.sheild <= 0:
            print(f"Life List = {len(life_list)}")
            if len(life_list) > 0:
                life_removal = life_list.pop()
                life_removal.kill()
                player.sheild = 100
                life_pos -= 30
                player.gun_type = 1

            else:
                player.kill()
                pygame.mixer.music.stop()
                all_sprites.remove(player)
                death_snd.play()
                alive = False
                with open("highscore", "r") as f:
                    hs = f.read()

                if score > int(hs):
                    with open("highscore", "w") as f:
                        f.write(str(score))


    elif hits and player.alive() and mystery.type == "bounus":
        score += 1000
        bounus_snd.play()

# Mob Hit Check

    hits = pygame.sprite.spritecollide(player, mobs, True, pygame.sprite.collide_circle)
    if hits and player.alive():
        print(f"Shield = {player.sheild}")
        print(f"Life List in hits = {len(life_list)}")
        for hit in hits:
            player.sheild -= random.randint(0, 30)
            if player.sheild < 1:
                player.sheild = 0
            death_snd.play()


    if hits and player.alive() and player.sheild <= 0:
        print(f"Life List = {len(life_list)}")
        if len(life_list) > 0:
            life_removal = life_list.pop()
            life_removal.kill()
            player.sheild = 100
            life_pos -= 30
            player.gun_type = 1

        else:
            player.kill()
            pygame.mixer.music.stop()
            all_sprites.remove(player)
            death_snd.play()
            alive = False
            with open("highscore","r") as f:
                hs = f.read()

            if score > int(hs):
                with open("highscore","w") as f:
                    f.write(str(score))

    all_sprites.update()

# Draw
    screen.fill(BLACK)
    screen.blit(background, background_rect)
    all_sprites.draw(screen)
    draw_text(screen, str(score), 28, WIDTH / 2, 10)
    draw_text(screen, f"Level {level}", 24, 42,13)
    draw_text(screen, f"High Score {highscore}",12,45,60)
    draw_sheild_bar(screen, 5, 5, player.sheild)
    if not alive:
        draw_text(screen, "Game Over", 48, WIDTH / 2, HEIGHT / 2)
    pygame.display.flip()

    if player.sheild < 100 and player.alive():
        now = pygame.time.get_ticks()
        if now - shield_time > 500:
            player.sheild += 1
            shield_time = now

    health_drop = random.randint(0, 1000)
    if health_drop == 1:
        health_up = Health()
        all_sprites.add(health_up)
        health_ups.add(health_up)

    bounus_drop = random.randint(0, 500)
    if bounus_drop == 1:
        bounus = Bounus()
        all_sprites.add(bounus)
        bounus_group.add(bounus)

    mystery_drop = random.randint(0, 250)
    if mystery_drop == 1:
        mystery = Mystery()
        all_sprites.add(mystery)
        mystery_group.add(mystery)

    gun_drop = random.randint(0, 200)
    if gun_drop == 1:
        if player.gun_type == 1:
            type_list = [2, 4]
            gun = Gun(type_list[random.randint(0, 1)])
            all_sprites.add(gun)
            gun_group.add(gun)
        elif player.gun_type == 2:
            gun = Gun(4)
            all_sprites.add(gun)
            gun_group.add(gun)





pygame.quit()
